"use strict";

var express = require("express");
var router = express.Router();
var { action } = require("../crawling/crawling.js");
const mysql = require("mysql");
const config = require("../configurations/databaseConfig");
var readData = require("../database/databaseFunctions").readData;

router.get("/refresh", function(req, res, next) {
    action();
    res.sendStatus(200);
});

router.get("/button", function(req, res, next) {
    const conn = new mysql.createConnection(config);
    conn.connect(function(err) {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                message: "could not create database connection"
            });
            return;
        } else {
            console.log("Connection established.");
        }
    });

    var currentDate = new Date();
    var dateFormat = require("dateformat");
    dateFormat(currentDate, "isoDateTime");

    var sql = "SELECT * FROM button";

    conn.query(sql, function(err, result, fields) {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                message: "could not run query"
            });
        } else {
            var dateTable = result[0]["data_actualization"];
            var difference = currentDate - dateTable;
            var diffMinutes = Math.round(
                ((difference % 86400000) % 3600000) / 60000
            );

            res.json({
                success: true,
                timeDifference: diffMinutes
            });
        }
    });
});

router.post("/button_pressed", function(req, res, next) {
    const conn = new mysql.createConnection(config);
    conn.connect(function(err) {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                message: "could not create database connection"
            });
            return;
        } else {
            console.log("Connection established.");
        }
    });
    var sql = "select data_actualization from button";
    var lastTimePressed;
    conn.query(sql, function(err, result, fields) {
        if (err) {
            console.log(err);
            res.json({
                success: false,
                message: "could not run query"
            });
            return;
        }
        lastTimePressed = result[0]["data_actualization"];
        var difference = new Date() - lastTimePressed;
        var diffMinutes = Math.round(
            ((difference % 86400000) % 3600000) / 60000
        );

        if (diffMinutes > 30) {
            sql = "UPDATE button SET data_actualization = ?";
            conn.query(sql, new Date(), function(err, result, fields) {
                if (err) {
                    console.log(err);
                    res.json({
                        success: false,
                        message: "could not update last time pressed"
                    });
                } else {
                    console.log("Updated!");
                    res.json({
                        success: true,
                        message: "updated"
                    });
                }
            });
        } else {
            res.json({
                success: false,
                message: "30 minutes have not passed yet"
            });
        }
    });
});

module.exports = router;